configure_file(
  biometryd.conf.in biometryd.conf @ONLY
)
configure_file(
  biometryd.pc.in biometryd.pc @ONLY
)

install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/biometryd.pc
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
  )

install(
  FILES ${CMAKE_CURRENT_SOURCE_DIR}/com.ubports.biometryd.Service.conf
  DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/dbus-1/system.d/
)

install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/biometryd.conf
  DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/init
)

if (USE_SYSTEMD)
    pkg_get_variable(SYSTEMD_SYSTEM_UNIT_DIR systemd systemdsystemunitdir)

    configure_file(
      biometryd.service.in biometryd.service @ONLY
    )

    install(
      FILES ${CMAKE_CURRENT_BINARY_DIR}/biometryd.service
      DESTINATION ${SYSTEMD_SYSTEM_UNIT_DIR}
    )
endif()
