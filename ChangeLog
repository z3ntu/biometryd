2024-01-25 Mike Gabriel

        * Release 0.3.1 (HEAD -> main, tag: 0.3.1)

2023-11-15 Ratchanan Srirattanamet

        * Merge branch 'fix/useGnuinstalldirsVariables' into 'main' (db870de)

2023-11-14 OPNA2608

        * Make it possible to opt out of systemd dependency (12890aa)
        * debian/control: Add build-time dependency on systemd (0a7881b)
        * Query systemd pkg-config for unit install dir (43de9ef)
        * data: Use configure_file instead of assuming installation paths
          (262ba38)
        * data/CMakeLists: Use GNUInstallDirs variables more (2470160)

2023-11-14 Ratchanan Srirattanamet

        * Merge branch 'fix/qt5_use_modules' into 'main' (a3e8d0c)

2023-11-14 OPNA2608

        * Stop using qt5_use_modules (9e52fad)

2023-11-10 Marius Gripsgard

        * Merge branch 'missing-headers' into 'main' (bc6f1a7)

2023-11-04 Brandon Boese

        * qml/Biometryd/user.cpp: Add missing include for <sys/types.h>
          (627ddb3)
        * Add missing headers for gcc 13 (745ae06)

2023-01-30 Mike Gabriel

        * Merge branch 'personal/gberh/dh-12' into 'main' (2878a23)

2023-01-30 Guido Berhoerster

        * Update to dh version 12 (5511d47)

2023-01-19 Mike Gabriel

        * Release 0.3.0 (3d3ed4e) (tag: 0.3.0)

2023-01-10 Mike Gabriel

        * Merge branch 'personal/mariogrip/fixnohybris' into 'main' (a8818c2)

2023-01-10 Marius Gripsgard

        * src: Fix building without hybris (1343656)
        * Set libhybris as required if WITH_HYBIRS option is set on (f57fdda)

2023-01-05 Guido Berhoerster

        * Merge branch 'personal/sunweaver/fix-ENABLE-WERROR-CMake-option'
          into 'main' (872dfab)

2023-01-04 Mike Gabriel

        * d/rules: When building for UBports repos, enable -Werror via CMake
          option. (78cb1f2)
        * CMakeLists.txt: Don't hard-core -Werror anymore. We have a CMake
          option for this now. (3af8aee)

2023-01-05 Guido Berhoerster

        * Merge branch 'personal/sunweaver/set-project-version' into 'main'
          (e7e862b)

2023-01-04 Mike Gabriel

        * CMakeLists.txt: Set PROJECT_VERSION, so that our release script
          does not pick the SOVERSION numbers instead. (2a81ead)
        * CMake: Bump mininum version requirement to 3.5 (bde35e4)

2022-12-12 Marius Gripsgard

        * Merge branch 'personal/fredldotme/android9forwardport' into 'main'
          (d01cfd3)

2022-12-12 Alfred Neumayer

        * debian & CMake: Fix WITH_HYBRIS option and use (bca9c46)

2022-12-11 Alfred Neumayer

        * CMake: Disable ASIO epoll (da1e677)
        * debian: Enable hybris support (a5800b5)
        * CMake: Turn hybris support into opt-in affair (2e95231)

2022-12-09 Alfred Neumayer

        * tests: Increase timing by 1 sec (fade729)

2022-07-02 Alfred Neumayer

        * Reduce DBus timeouts (312ecda)

2022-01-25 Florian Leeber

        * We only need it for the size operation. If applied to list
          operation it blocks print deletion in UI (4377eab)

2022-01-24 Florian Leeber

        * Fix missing initialization (e81dba0)

2022-01-23 Florian Leeber

        * Fix compile (d8a03e2)
        * set => enable for the enable method name :) (7416313)
        * Whitespace (65c26f9)

2022-01-22 Florian Leeber

        * Cleanup (019e5ed)

2022-01-19 Florian Leeber

        * Cleanup (9cda308)
        * More debug (e2f674f)
        * Try with constructor (36e866f)
        * Bugfix (fbe7824)
        * Try to fix the boolean var (6c4d47b)

2022-01-18 Florian Leeber

        * Debugging only (ef52920)
        * Add a quirks mechanism to pretend that 1 finger is stored (d419db6)

2021-05-13 Ratchanan Srirattanamet

        * Move Jenkinsfile to debian/ per the new guideline (4ba8fc4)

2020-05-14 Erfan Abdi

        * Revert "[TMP] Drop -Werror" (1986b84)

2020-05-12 Erfan Abdi

        * We’re UBports foundation (d409cea)
        * Drop unused header (097472b)

2020-05-10 Erfan Abdi

        * Find TmpDir based on device first api level (8bf3e39)

2020-05-09 Erfan Abdi

        * Hardcode UserID on authenticate (e5e65dc)
        * Revert "Verify UserID on authenticated" (8ef5e8e)
        * Fix List and size when there’s only one finger (deac945)
        * Verify UserID on authenticated (121a2cd)
        * Set real User ID on enroll (b382bb7)
        * Fail if finger not recognized (01ba7b7)
        * Separate Authentication operations (0bba9d2)
        * Clear list before enumerate (10e28f4)
        * Filter finger 0 authenticated (e7460a3)
        * Fix Enroll and List percentage (4b76953)
        * Use gatekeeper to verify and get hat (f471c2f)
        * Fix FP service not avaliable error (d3bf06d)
        * Combine preEnroll to Enroll (157f65b)
        * Revert "Auth with keystore" (2460525)

2020-05-07 Erfan Abdi

        * Allow to re-setNotify (8da850d)
        * Fix wrong callback on Authenticate Operation (5a5d5e8)
        * Drop androidFailedOperation (46081a3)
        * Remove finger id 0 is clear all (e591503)
        * Auth with keystore (6425829)
        * Use network byte for auth key (da2476a)

2020-03-29 Erfan Abdi

        * print debugings (1a0af23)

2020-05-06 Erfan Abdi

        * Suppress warnings (af80d36)

2018-02-19 Marius Gripsgard

        * Disable tests (for now) (9e2885a)

2020-03-26 Erfan Abdi

        * Implement android hal wrapper as device (d3c6951)
        * Setup hybris bridge for libbiometry_fp_api.so (a21452e)

2020-03-25 Erfan Abdi

        * Move observer to android.cpp (24e08fd)
        * Complete android part of api (1d0b0e3)
        * Add very basic android hybris fp implementation (59bb8cf)

2020-03-24 Erfan Abdi

        * Use android hal wrapper as default device (fafcf76)
        * create null device (a8b8678)
        * set configuration for enroll (b9fc17a)
        * Fix device arg duplicate in enroll (7a8ba11)

2020-03-23 Erfan Abdi

        * Add android device (f496235)
        * Add Cmake gitignore (98873c9)

2018-02-19 Marius Gripsgard

        * Fix gcc 7 (1880159)

2022-11-25 Ratchanan Srirattanamet

        * Merge branch
          'personal/sunweaver/prepare-release-plus-debian-folder-sync-from-debian'
          into 'main' (4d8024d)

2022-10-04 Mike Gabriel

        * debian/: Sync over packaging improvements from official Debian
          upload. (0e1bfaa)

2022-10-04 Guido Berhoerster

        * Merge branch 'personal/sunweaver/enable-Werror-cmake-option' into
          'main' (3d7106c)

2022-10-03 Mike Gabriel

        * CMake: Add ENABLE_WERROR boolean build option. (637978d)
        * Merge branch 'fix-compile' into 'main' (39b529a)

2022-10-04 Jami Kettunen

        * util/dynamic_library: Add missing include for <memory> (3c07f09)

2022-10-03 Mike Gabriel

        * Merge branch 'systemd-migration' into 'main' (3c10032)

2022-08-15 Guido Berhoerster

        * Update packaging metadata (e7aa6bf)

2022-08-12 Guido Berhoerster

        * Remove obsolete snap packaging (5b4847c)
        * Remove obsolete bzr-builddeb files (e5f3b09)
        * Add systemd service file (94b5f77)

2022-09-06 Guido Berhoerster

        * Indicate missing device configuration through exit code and prevent
          respawn (a5e8c27)
        * Revert "Prevent from restarting in case of missing device
          configuration." (2d47aa3)

2021-07-21 Rodney

        * Merge branch 'personal/peat-psuwit/no-cross-qmltypes' into 'main'
          (9cf1e18)

2021-05-19 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/stub-worker-threads' into 'main'
          (5c7be3b)

2021-04-23 Ratchanan Srirattanamet

        * service: make sure there's a worker thread for stubs (747c535)
        * qml: don't generate qmltypes when cross compiling (18d4cda)

2021-05-14 Rodney

        * Merge branch 'personal/peat-psuwit/make-it-multidist' into 'main'
          (ce19b7c)

2021-05-13 Ratchanan Srirattanamet

        * tests: replace stub's Runtime with a simple worker thread (64c4cbf)

2021-05-12 Ratchanan Srirattanamet

        * Rename DBus namespaces to com.ubports (dd8f0c9)

2021-04-23 Ratchanan Srirattanamet

        * tests: replace skeleton's Runtime with a simple worker thread
          (5cbb29a)

2021-04-01 Ratchanan Srirattanamet

        * tests: build GMock as shared libraries (394467e)

2021-03-31 Ratchanan Srirattanamet

        * runtime: narrow the pool_size's type to uint16_t (b0d664d)
        * d/control: update QML B-D to use qml-module format (b7efe2c)

2021-05-12 Ratchanan Srirattanamet

        * Move Jenkinsfile to debian/ per the new guideline (ee9fe9f)

2021-02-08 Rodney

        * Merge pull request #4 from ubports/xenial_-_crash-less (dc1c73e)

2021-02-06 Marius Gripsgard

        * tests: Do not build empty test (8dd8d9a)
        * tests: Include gmock headers (3432736)
        * Update jenkinsfile (ccabeed)
        * debian: Cleanup debian pkg cruft (b5297dc)

2018-02-19 Marius Gripsgard

        * Build in parallel (d2c80ca)

2021-02-02 Marius Gripsgard

        * Do not create runtime for dbus stubs (5909653)
        * Fix newer gcc by specifying type (8512c42)
        * Add missing includes (54c8a92)

2018-02-20 Dan Chapman

        * Update changelog (1e82bbe)

2018-01-07 Dan Chapman

        * Imported to UBports (f648b3b)

2017-04-06 Bileto Bot

        * Releasing 0.0.1+17.04.20170406.2-0ubuntu1 (f354459)

2017-04-06 Łukasz 'sil2100' Zemczak

        * Switch to using cmake-extras for gmock and gtest. (LP: #1680153)
          (9c57822)
        * The leftover add_subdirectory of gmock makes CMake still bail out.
          Thanks to Pete Woods\! (912138f)
        * First try on fixing the gmock related build failure. (3d36b76)

2016-09-22 Bileto Bot

        * Releasing 0.0.1+16.10.20160922.3-0ubuntu1 (3a56b6c)

2016-09-22 Ken VanDine

        * Added shlibs:Depends;  Use split mode instead of native (0e66461)
        * Skip qml test on powerpc, segfaults. LP: #1606927;  Cast enum to
          int, to use in another enum. Fixes FTBFS with gcc6.
          Rebuild for boost soname change.;  No-change rebuild for
          boost soname change. (4ab404d)

2016-09-21 Ken VanDine

        * dropped symbols file (fb57dbd)
        * Added symbols file (516fa8f)
        * Added shlibs:Depends (44d4687)
        * Use split mode instead of native (08f667c)

2016-06-28 Bileto Bot

        * Releasing 0.0.1+16.10.20160628-0ubuntu1 (71afe63)

2016-06-28 Thomas Voß

        * Add sphinx documentation. (58823c7)
        * Add an initial snapcraft.yaml setup for biometryd. (16f393a)

2016-06-27 Bileto Bot

        * Releasing 0.0.1+16.10.20160627.2-0ubuntu1 (66ad960)

2016-06-27 Thomas Voß

        * If default device cannot be instantiated, exit cleanly with success
          to prevent respawn watchdog from triggering. (34ab5f6)
        * Prevent from restarting in case of missing device configuration.
          (3de1a30)
        * Fix up test case. (4058d87)
        * If default device cannot be instantiated, exit cleanly with success
          to prevent respawn watchdog from triggering. (b6aa3fa)

2016-06-26 Thomas Voß

        * [ Thomas Voß ];  Verify incoming requests. (LP: #1593383.
          Immediately cancel operations instead of enqueuing
          cancellation.;  Only for landing purposes.;  cmds::Run now
          tries to make an educated guess for configuring the
          default device.;  Also consider
          /custom/vendor/biometryd/plugins when scanning for
          plugins. [ Ken VanDine ];  Added COPYING file for the
          LGPL-3 and updated copyright for the   single file
          licensed under MIT (a085d8b)

2016-06-24 Thomas Voß

        * [ Thomas Voß ];  Verify incoming requests. (LP: #1593383.
          Immediately cancel operations instead of enqueuing
          cancellation.;  Only for landing purposes.;  cmds::Run now
          tries to make an educated guess for configuring the
          default device.;  Also consider
          /custom/vendor/biometryd/plugins when scanning for
          plugins. [ Ken VanDine ];  Added COPYING file for the
          LGPL-3 and updated copyright for the   single file
          licensed under MIT (9dad138)
        * Address reviewer comments. (c76064e)

2016-06-23 Thomas Voß

        * Add an initial snapcraft.yaml setup for biometryd. (ea05662)

2016-06-21 Bileto Bot

        * Releasing 0.0.1+16.10.20160621.1-0ubuntu1 (3e3a158)

2016-06-21 Thomas Voß

        * Also consider /custom/vendor/biometryd/plugins when scanning for
          plugins. (8ffb015)
        * cmds::Run now tries to make an educated guess for configuring the
          default device. Install a default upstart configuration.
          (08e0a70)

2016-06-21 Ken VanDine

        * Added COPYING file for the LGPL-3 and updated copyright for the
          single file licensed under MIT (423ff3d)

2016-06-21 Thomas Voß

        * Only for landing purposes. (fdefc71)
        * Immediately cancel operations instead of enqueuing cancellation.
          (575947b)
        * Verify incoming requests. (LP: #1593383) (1b60f68)
        * Make sure that we consider all configured plugin directories
          instead of returning early. (94799c4)
        * Add missing break on switch for cmds::Config::Flag. (a0a054d)
        * Also consider /custom/vendor/biometryd/plugins when scanning for
          plugins. (233abee)

2016-06-20 Thomas Voß

        * Limit respawn attempts to 10. (c79b730)
        * cmds::Run now tries to make an educated guess for configuring the
          default device. Install a default upstart configuration.
          (2f18eff)

2016-06-19 Thomas Voß

        * Add images illustrating key system aspects. (3bfa863)
        * Adjust author/copyright holder. (0e901c6)
        * Leave comment about documentation being generated in the source
          tree. (f989e25)
        * Add section on cli for testing. (14d626c)
        * Refactor doc folder setup. (e297623)
        * Install breathe. (6aada40)
        * Adjust input pathes. (97bbdb3)
        * Add Doxyfile to rtd source folder. (ed5d578)
        * Adjustments for builds on readthedocs.org. (88ee2aa)
        * Add sphinx documentation. (27b8ae6)

2016-06-17 Ken VanDine

        * Added COPYING file for the LGPL-3 and updated copyright for the
          single file licensed under MIT (19fbf1c)

2016-06-17 Thomas Voß

        * Add a manual test plan. (12e788f)
        * Immediately cancel operations instead of enqueuing cancellation.
          (bc1d0ff)
        * Make sure that test cases pass if run in virtualized environments
          without securityfs being mounted. (8d94813)
        * Verify incoming requests. (1b729e6)

2016-06-15 Thomas Voß

        * Fix a race on cancellation/destruction with the
          Observer::on_cancelled implementation accessing a dangling
          QObject pointer. (3483e18)
        * Support List/Removal operations in qml bindings. (13d9a26)
        * Merge trunk. (2abefcd)
        * Merge lp:~thomas-voss/biometryd/robustify-comms/ (d4266fa)

2016-06-14 Thomas Voß

        * Merge trunk and apply comm fixes to newly added methods. (e87149b)
        * Merge lp:~thomas-voss/biometryd/extend-template-store-interface
          (e85048e)
        * Fix up test cases and make sure that TemplateStore::list and
          TemplateStore::remove is exercised. (0066928)
        * Extend biometry::TemplateStore:   * list lists all known templates
          by id.   * remove erases a specific template by id from
          the template store. (411de0b)

2016-06-13 Thomas Voß

        * Dummy enrollment/identification operations now take 5 seconds in
          total. (3d37cff)

2016-06-07 Thomas Voß

        * Robustify comms with the service side of things. Adjust cmds::Test
          to account for async operations. (ba27d41)

2016-06-06 Thomas Voß

        * Merge
          lp:~thomas-voss/biometryd/test-against-service-if-no-config-given
          (a34dde2)
        * Test against actual service if no explicit config is given.
          (2ce3a1f)

2016-06-02 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/calculate-statistics-for-cmd-test
          (1e55d26)
        * Calculate timing statistics for fingerprint identification.
          (6e6d3bd)

2016-05-31 Thomas Voß

        * Gracefully handle the service not being present in QML bindings.
          (6a1ca5a)

2016-05-25 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/fix-y-build (ea4cc49)
        * Fix Y build. (8df03f8)
        * Switch to QRectF. (d424dde)
        * Merge trunk. (ddf626f)
        * Merge lp:~thomas-voss/biometryd/floating-point-mask-coordinates
          (b158c38)
        * Switch to a normalized coordinate system for communicating masks.
          (496e050)

2016-05-24 Thomas Voß

        * Fix typo. (b0a0d7b)
        * QRect assumes (x,y) and (w,h) on construction. (6f7ea46)

2016-05-19 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/identify-in-a-loop-for-test
          (432fe03)
        * Adjust doc string. (0d85ed5)
        * For cmds::Test, enable multiple independent trials for identifying
          the user. (1619698)

2016-05-17 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/beautify-pretty-print (62b6364)
        * Beautify terminal output for biometry::TracingOperationObserver<T>.
          (5d98ded)
        * Merge lp:~thomas-voss/biometryd/fix-properties-in-qml-bindings
          (f4f3a3b)
        * Merge
          lp:~thomas-voss/biometryd/pretty-print-details-in-tracing-observer
          (c52dbc2)
        * Add pretty printing of actual values passed to
          TracingObserver<T>::*. (242fff9)

2016-05-13 Thomas Voß

        * Mark properties as constant/changing as appropriate. (02da630)

2016-05-11 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/add-dbus-config (7a1d8ca)
        * Add data/com.ubuntu.biometryd.Service.conf (32d3410)
        * Add and install dbus configuration file for biometryd. (60d879c)
        * Merge lp:~thomas-voss/biometryd/fix-qml-module-installation-path
          (f2fe8ee)
        * Fix qml module installation path. (3fe9ea4)
        * Merge
          lp:~thomas-voss/biometryd/ensure-that-biometryd-bin-is-pulled-in
          (a586b19)
        * Ensure that biometryd-bin is pulled in. (5b52d81)
        * Merge lp:~thomas-voss/biometryd/fix-patch-version (f2dfa2c)
        * Fix typo in CMakeLists.txt. (5f7710d)
        * Merge lp:~thomas-voss/biometryd/add-test-command (b36c78c)
        * Add documentation. Add test for overall daemon functionality.
          Introduce explicit operator bool() const to
          util::Configuration::Node. (0d0a68c)
        * Add a command biometryd test --config=[path] --user=[id]. (962d1cd)
        * Merge lp:~ŧhomas-voss/biometryd/add-config-command (79d30c8)

2016-05-10 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/fix-powerpc-build (4d8d5f5)
        * Fix linker issue on powerpc 32bit builds on vivid. (0c416f4)
        * Add a command biometryd config --flag=*. (56f6ab5)
        * Merge lp:~thomas-voss/biometryd/restructure-cli (2b01abb)
        * Alter cli::* interface and provide a cli::Command::Context to run
          invocations. Restructure setup of default help options and
          commands. (a288648)

2016-05-09 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/factor-out-cli (5af0315)
        * Factor out Daemon::Command and friends to util::cli::*. (8d13dd0)
        * Merge lp:~thomas-voss/biometryd/remove-hybris-dependency (2deac46)
        * Remove hybris dependency. (198f60a)
        * Merge
          lp:~thomas-voss/biometryd/populate-registry-from-well-known-directories
          (02244a9)
        * Add test cases covering device registration on startup. (9773504)
        * biometry::Daemon now populates the list of known devices from a
          well-known plugin directory. (c35145e)

2016-05-07 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/introduce-qml-module (edc3168)
        * Introduce qml-module-biometryd. (f9d12a2)

2016-05-05 Thomas Voß

        * Make the testing backend stateful and keep track of number of
          enrolled templates. (4383db7)

2016-05-04 Thomas Voß

        * Merge lp:~thomas-voss/biometryd/fix-pkgconfig-setup (4aca051)
        * Fix pkgconfig setup. (568c526)
        * Merge
          lp:~thomas-voss/biometryd/register-plugin-device-with-registry
          (33e7042)
        * Make sure that devices::plugin::* is known to the device registry.
          (bcfcff0)
        * Correctly propagate result of operations to
          qml::Biometryd::Observer. Correctly propagete
          FingerprintReader::is_finger_present hint to
          qml::Biometryd::Observer. (b2c5e71)
        * Merge
          lp:~thomas-voss/biometryd/remove-obsolete-configuration-header
          (48b11f3)
        * Remove obsolete entry for biometry/configuration.h (8cd62b6)
        * Merge lp:~thomas-voss/biometryd/add-run-cmd (6468276)
        * Move around some files and pull in util to biometry::. Implement
          cmds::Run and add test cases covering its functionaltiy.
          (6051c2b)

2016-05-03 Thomas Voß

        * Expose additional hints for fingerprint readers. Add documentation
          to qml test case. (45b7a1d)

2016-05-02 Thomas Voß

        * Factor FingerprintReaderHints . (981d782)
        * Add a qml::for_testing::* stack that enables users of the QML
          bindings to test async behavior. Factor out conversion
          from biometry::*  to biometry::qml::* types. (3a2470a)
        * Fix up pkgconfig file. (1ffa350)
        * Initial commit. (b3c49ba)
